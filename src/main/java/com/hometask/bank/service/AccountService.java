package com.hometask.bank.service;

import com.hometask.bank.dto.RequestDTO;
import com.hometask.bank.dto.ResponseDTO;
import com.hometask.bank.dto.Source;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AccountService {

    @Autowired
    private Environment environment;

    private List<String> sourceList = new ArrayList<>();

    public ResponseDTO validSource(RequestDTO requestDTO) {

        ResponseDTO responseDTO = new ResponseDTO();
        List<Source> sourceList = new ArrayList<>();
        for(String sourceInput: requestDTO.getSources()){
            Source source = new Source();
            String URI = environment.getProperty("sources."+sourceInput,"");
            if(!"".equalsIgnoreCase(URI)){
                source.setSource(sourceInput);
                source.setValid(getResult(requestDTO.getAccountNumber(), URI).getBody());
                sourceList.add(source);
            }
        }
        responseDTO.setResult(sourceList);
        return responseDTO;
    }

    private ResponseEntity<Boolean> getResult(String accountNumber, final String uri){
//        if(uri.contains("v1")){
//            return true;
//        }else{
//            return false;
//        }
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        Map<String, String> params = new HashMap<String, String>();
        params.put("accountNumber", accountNumber);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(uri, HttpMethod.GET, entity, Boolean.class, params);
    }
}
