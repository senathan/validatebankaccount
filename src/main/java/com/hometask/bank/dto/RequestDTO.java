package com.hometask.bank.dto;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@EntityScan
public class RequestDTO {

    @NotBlank(message = "AccountNumber is mandatory")
//    @Size(message = "AccountNumber should be 9 digit", max = 9, min = 9)
    private String accountNumber;
    List<String> sources = new ArrayList<String>();

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public List<String> getSources() {
        return sources;
    }

    public void setSources(List<String> sources) {
        this.sources = sources;
    }
}
