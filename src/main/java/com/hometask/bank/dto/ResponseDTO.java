package com.hometask.bank.dto;

import java.util.ArrayList;
import java.util.List;

public class ResponseDTO {

    List<Source> result = new ArrayList<Source>();

    public List<Source> getResult() {
        return result;
    }

    public void setResult(List<Source> result) {
        this.result = result;
    }
}
