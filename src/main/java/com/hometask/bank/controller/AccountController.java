package com.hometask.bank.controller;

import com.hometask.bank.dto.RequestDTO;
import com.hometask.bank.dto.ResponseDTO;
import com.hometask.bank.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;

import java.util.HashMap;
import java.util.Map;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    /**
     * Return the account information from source V1.
     *
     * @return the account
     */
    @ResponseBody
    @RequestMapping(method = RequestMethod.POST, value = "/api/validate")
    public ResponseDTO getAccountInfoV1(@Valid @RequestBody RequestDTO requestDTO) {

        ResponseDTO responseDTO = accountService.validSource(requestDTO);
        return responseDTO;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
