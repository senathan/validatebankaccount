package com.hometask.bank;

import com.hometask.bank.dto.RequestDTO;
import com.hometask.bank.dto.ResponseDTO;
import com.hometask.bank.dto.Source;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = BankApplication.class,
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BankApplicationIntegrationTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
	@Test
	void contextLoads() {
	}

	@Test
	public void testValidAccountNumber() {
		RequestDTO requestDTO = new RequestDTO();
		requestDTO.setAccountNumber("12345678");
		List<String> sourceList  = new ArrayList<String>();
		sourceList.add("source1");
		sourceList.add("source2");
		requestDTO.setSources(sourceList);
		ResponseEntity<ResponseDTO> responseEntity = this.restTemplate
				.postForEntity("http://localhost:" + port + "/api/validate", requestDTO, ResponseDTO.class);
		Assertions.assertEquals(201, responseEntity.getStatusCodeValue());
	}

	@Test
	public void testInValidAccountNumber() {
		RequestDTO requestDTO = new RequestDTO();
		List<String> sourceList  = new ArrayList<String>();
		sourceList.add("source1");
		sourceList.add("source2");
		requestDTO.setSources(sourceList);
		ResponseEntity<ResponseDTO> responseEntity = this.restTemplate
				.postForEntity("http://localhost:" + port + "/api/validate", requestDTO, ResponseDTO.class);
		Assertions.assertEquals(400, responseEntity.getStatusCodeValue());
	}

}
